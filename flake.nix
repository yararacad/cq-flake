{
  description = "CQ-editor and CadQuery";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;
    flake-utils.url = "github:numtide/flake-utils";
    cadquery-src = {
      url = "github:CadQuery/cadquery";
      flake = false;
    };
    cq-editor-src = {
      url = "github:CadQuery/CQ-editor";
      flake = false;
    };
    ocp-src = {
      url = "github:cadquery/ocp";
      flake = false;
    };
    ocp-stubs-src = {
      url = "github:cadquery/ocp-stubs";
      flake = false;
    };
    pywrap-src = {
      url = "github:CadQuery/pywrap";
      flake = false;
    };
    llvm-src = {
      url = "github:llvm/llvm-project/llvmorg-10.0.1";
      flake = false;
    };
    pybind11-stubgen-src = {
      url = "github:CadQuery/pybind11-stubgen";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, ... } @ inputs:
      flake-utils.lib.eachSystem [ "x86_64-linux" ]( system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
          # keep gcc, llvm and stdenv versions in sync
          gccSet = {
            # have to use gcc9 because freeimage complains with gcc8, could probably build freeimage with gcc8 if I have to, but this is easier.
            llvmPackages = pkgs.llvmPackages_10; # canonical now builds with llvm10: https://github.com/CadQuery/OCP/commit/2ecc243e2011e1ea5c57023dee22e562dacefcdd
            stdenv = pkgs.gcc9Stdenv;
          };
          # I'm quite worried about how I handle this VTK. Python -> VTK (for Python bindings) -> OCCT -> Python(OCP)
          new_vtk_9 = pkgs.libsForQt5.callPackage ./expressions/VTK { enablePython = true; pythonInterpreter = python; };
          opencascade-occt = pkgs.callPackage ./expressions/opencascade-occt {
            inherit (gccSet) stdenv;
            vtk_9 = new_vtk_9;
          };
          nlopt = pkgs.callPackage ./expressions/nlopt.nix {
            inherit python;
            pythonPackages = python.pkgs;
          };
          py-overrides = import expressions/py-overrides.nix {
            inherit gccSet;
            inherit (inputs) llvm-src pywrap-src ocp-src ocp-stubs-src cadquery-src pybind11-stubgen-src;
            inherit (pkgs) fetchFromGitHub;
            vtk_9_nonpython = new_vtk_9;
            occt = opencascade-occt;
            nlopt_nonpython = nlopt;
          };
          python = pkgs.python38.override {
            packageOverrides = py-overrides;
            self = python;
          };
          cq-kit = python.pkgs.callPackage ./expressions/cq-kit.nix {};

        in rec {
          packages = {
            cq-editor = pkgs.libsForQt5.callPackage ./expressions/cq-editor.nix {
              python3Packages = python.pkgs // { inherit cq-kit; };
              src = inputs.cq-editor-src;
            };
            cq-docs = python.pkgs.callPackage ./expressions/cq-docs.nix {
              src = inputs.cadquery-src;
            };
            cadquery-env = python.withPackages (
              ps: with ps; [ cadquery cq-kit python-language-server black mypy ocp-stubs pytest pytest-xdist pytest-cov pytest-flakefinder pybind11-stubgen ]
            );
            cadquery-libs = (ps: with ps; [ cadquery cq-kit ]);

            just-ocp = python.withPackages ( ps: with ps; [ ocp ] );
            inherit python opencascade-occt;
          };

          defaultPackage = packages.cadquery-libs;
          defaultApp = {
            type = "app";
            program = packages.cq-editor + "/bin/cq-editor";
          };
          overlays = { inherit py-overrides; };
          # TODO: add dev env for cadquery
          # devShell = packages.cadquery-dev-shell;
          # TODO: add dev env for cq-editor, with hopefully working pyqt5
        }
      );
}
